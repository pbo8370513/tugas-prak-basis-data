#Instalasi Basis Data

## No.1

Berikut adalah langkah-langkah cara menginstal mengkonfigurasi basis data relasional PostgreSQL di Windows.



Unduh installer PostgreSQL dari situs web EDB di https://www.enterprisedb.com/downloads/postgres-postgresql-downloads. Pilih versi yang sesuai dengan sistem operasi Windows Anda.

![image](https://gitlab.com/pbo8370513/tugas-prak-basis-data/-/raw/main/Screenshot__361_.png)

Jalankan installer yang telah diunduh. Ikuti petunjuk yang diberikan oleh installer untuk menginstal PostgreSQL di komputer Anda.

Selama proses instalasi, Anda akan diminta untuk memasukkan beberapa informasi konfigurasi, seperti username dan password untuk superuser database (biasanya "postgres"). Pastikan untuk mencatat informasi ini.

Setelah instalasi selesai, buka aplikasi pengelola basis data seperti DBeaver atau pgAdmin.

Dalam aplikasi pengelola basis data, buat koneksi baru dengan mengisi informasi berikut:

Host: Biasanya "localhost" jika PostgreSQL diinstal di komputer yang sama. Jika PostgreSQL diinstal di komputer lain, masukkan alamat IP atau nama host komputer tersebut.
Port: Biasanya "5432" (port default PostgreSQL).
Username: Isi dengan username superuser yang Anda tetapkan saat instalasi.
Password: Masukkan password yang Anda tetapkan saat instalasi.
Setelah mengisi informasi koneksi, coba tes koneksi ke server PostgreSQL menggunakan tombol "Test Connection" atau sejenisnya di aplikasi pengelola basis data. Pastikan koneksi berhasil dan tidak ada kesalahan yang ditampilkan.
